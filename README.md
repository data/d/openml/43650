# OpenML dataset: Spike-waveforms

https://www.openml.org/d/43650

## Structure

The dataset has the following file structure:

* `dataset/`
  * `tables/`
    * [`data.pq`](./dataset/tables/data.pq): Parquet file with data
  * [`metadata.json`](./dataset/metadata.json): OpenML description of the dataset
  * [`features.json`](./dataset/features.json): OpenML description of table columns
  * [`qualities.json`](./dataset/qualities.json): OpenML qualities (meta-features)

## Description

Introduction
Neurons in the brain use electrical signals communication. Different spike waveforms correspond to different cell types or different neuron morphologies (Henze et al., J. Neurophysiol. 2000). In learning about different spike waveforms, we may identify different neuronal types in our sample.
Content

The data contains 150 numerical categories (0 to 149, in columns) corresponding to the sampling of a 5-millisecond extracellular voltage at 30 kHz. Since the extracellular voltage amplitude related to the squared of the distance to the cell, we normalize the whole trace to the minimal voltage (through) that corresponds to the peak of the spike. To load it in Python.
import pandas as pd
waveforms = pd.read_csv('waveforms.csv', index_col = 'uid')
waveforms.info()
waveforms.iloc[0, :-1,].plot()  plot the first waveform (last column is organoid)

To know more
For more information visiy my GitHub repository

## Contributing

This is a [read-only mirror](https://gitlab.com/data/d/openml/43650) of an [OpenML dataset](https://www.openml.org/d/43650). Contribute any changes to the dataset there. Alternatively, [fork the dataset](https://gitlab.com/data/d/openml/43650/-/forks/new) or [find an existing fork](https://gitlab.com/data/d/openml/43650/-/forks) to contribute to.

You can use [issues](https://gitlab.com/data/d/openml/43650/-/issues) to discuss the dataset and any issues.

For more information see [https://datagit.org/](https://datagit.org/).

